﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    class PessoaJuridica : Pessoa
    {
        public List<PessoaFisica> Socios { get; set; }
        public int Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantazia { get; set; }
        public string InscrEstadual { get; set; }
        public DateTime DataAbertura { get; set; }
        public int Idade
        {
            get
            {
                return Auxiliar.CalculaIdade(DataAbertura);
            }
        }
        public double Faturamento { get; set; }

        public PessoaJuridica(int id, string endereco, string tel,
        string email, List<PessoaFisica> socios, int cnpj,
        string razaoSocial, string nomeFantazia, string inscrEstadual,
        DateTime dataAbertura, double faturamento) : base(id, endereco, tel, email)
        {
            Socios = socios;
            Cnpj = cnpj;
            RazaoSocial = razaoSocial;
            NomeFantazia = nomeFantazia;
            InscrEstadual = inscrEstadual;
            DataAbertura = dataAbertura;
            Faturamento = faturamento;
        }
    }
}
