﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    class ContaSalario : Conta
    {
        public ContaSalario(Pessoa titular, long numero, int agencia,
            double saldo) : base(titular, numero, agencia, saldo)
        {
        }

        public override void Sacar(double valor)
        {
            double saldo = ConsultarSaldo();
            double valorSaque = valor - TaxaSaque;

            if (saldo > valorSaque)
            {
                saldo -= valorSaque;
            }
            else
            {
                Console.WriteLine("O valor de saque é indisponível");
            }
        }
    }
}
