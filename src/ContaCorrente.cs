﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    class ContaCorrente : Conta, Depositavel
    {
        public string Tipo
        {
            get
            {
                return "CONTA_CORRENTE";
            }
        }
        public double Limite { get; }
        public double TaxaDoLimite { get; }

        public ContaCorrente(Pessoa titular, long numero, int agencia,
            double saldo) : base(titular, numero, agencia, saldo)
        {

        }

        public void Pagar(string codigoBarras)
        {

        }

        public void Emprestimo(double valor)
        {

        }

        public override void Sacar(double valor)
        {
            double saldo = ConsultarSaldo();
            double valorSaque = valor - TaxaDoLimite;
            saldo -= valorSaque;
        }

        public void Depositar(double valor)
        {
            double saldo = ConsultarSaldo();
            saldo += valor;
        }
    }
}
