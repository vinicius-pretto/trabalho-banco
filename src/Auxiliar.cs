﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    static class Auxiliar
    {
        static public int CalculaIdade(DateTime data)
        {
            DateTime currentDate = DateTime.Now;
            int years = currentDate.Year - data.Year;

            if ((currentDate.Month < data.Month) ||
                (currentDate.Month == data.Month && currentDate.Day < data.Day))
            {
                return years - 1;
            }
            return years;
        }

        static public string FaixaEtaria(int idade)
        {
            if (idade <= 11)
            {
                return "Criança";
            }
            else if (idade >= 12 && idade <= 21)
            {
                return "Jovem";
            }
            else if (idade >= 22 && idade <= 59)
            {
                return "Adulto";
            }
            else
            {
                return "Idoso";
            }
        }
    }
}
