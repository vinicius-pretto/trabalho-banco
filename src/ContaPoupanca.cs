﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    class ContaPoupanca : Conta, Depositavel
    {
        public ContaPoupanca(Pessoa titular, long numero, int agencia,
            double saldo) : base(titular, numero, agencia, saldo)
        {

        }

        public override void Sacar(double valor)
        {
            double saldo = ConsultarSaldo();
            double valorSaque = valor - TaxaSaque;

            if (saldo > valorSaque)
            {
                saldo -= valorSaque;
            }
            else
            {
                Console.WriteLine("O valor de saque é indisponível");
            }
        }
        public void Depositar(double valor)
        {
            double saldo = ConsultarSaldo();
            saldo += valor;
        }
    }
}
