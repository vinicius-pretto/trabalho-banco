﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    abstract class Pessoa
    {
        public static int NumeroDePessoas { get; }
        public int Id { get; set; }
        public string Endereco { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public Pessoa(int id, string endereco, string tel, string email)
        {
            Id = id;
            Endereco = endereco;
            Tel = tel;
            Email = email;
        }
    }
}
