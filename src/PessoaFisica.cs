﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    class PessoaFisica : Pessoa
    {

        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public string Rg { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNasc { get; set; }
        public double Renda { get; set; }
        public int Idade
        {
            get
            {
                return Auxiliar.CalculaIdade(DataNasc);
            }
        }
        private string FaixaEtaria
        {
            get
            {
                return Auxiliar.FaixaEtaria(Idade);
            }
        }

        public PessoaFisica(int id, string endereco, string tel,
        string email, string nome, string sobreNome,
        string rg, string cpf, DateTime dataNasc,
        double renda) : base(id, endereco, tel, email)
        {
            Nome = nome;
            SobreNome = sobreNome;
            Rg = rg;
            Cpf = cpf;
            DataNasc = dataNasc;
            Renda = renda;
        }
    }
}
