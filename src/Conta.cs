﻿using System;
using System.Collections.Generic;
using System.Text;

namespace src
{
    abstract class Conta
    {
        public Pessoa Titular { get; set; }
        public long Numero { get; set; }
        public int Agencia { get; set; }
        public double Saldo { get; }
        public double TaxaSaque
        {
            get
            {
                return 6;
            }
        }

        public Conta(Pessoa titular, long numero, int agencia, double saldo)
        {
            Titular = titular;
            Numero = numero;
            Agencia = agencia;
            Saldo = saldo;
        }

        abstract public void Sacar(double valor);

        public double ConsultarSaldo()
        {
            return Saldo;
        }

        public void Transferir(Conta conta, double valor)
        {

        }
    }
}
