﻿using System;

namespace src
{
    class Program
    {
        static void Main(string[] args)
        {
            PessoaFisica vinicius = new PessoaFisica(
                1,
                "endereco",
                "tel",
                "email",
                "nome",
                "sobreNome",
                "rg",
                "cpf",
                1000
            );

            Console.WriteLine("Nome: " + vinicius.Nome);
        }
    }
}
